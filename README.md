The assignment has been completed using Django. I have built API using Django Rest Framework.
The server folder contain all the mandatory file.
Django follows MVC structure(Model View Controller). 
The incoming request is first handled by middleware, there are already many in built middleware but you can make your custom middleware.
I have make a custom middleware TimeDelayMiddleware which introduces delay in the request if the api path is 'process/*'.
We define the tables in models.py . I have made a table name requestHistory which is added to the     
