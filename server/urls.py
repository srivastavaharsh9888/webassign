from django.conf.urls import url,include
from server.views import process,stat,StatDetail

urlpatterns = [
    url('^$',stat,name='stat'),
    url('process/',process,name='process'),
    url('stats/$',stat,name='show-stat'),
    url('stats/detail/$',StatDetail.as_view(),name='stat-detail')
]