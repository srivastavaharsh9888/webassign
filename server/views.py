import datetime
from django.shortcuts import render
from django.db.models import Count,Avg,Sum
from django.utils import timezone

#imports from django-rest-framework
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status, authentication, exceptions,serializers

from server.utils import allheaders
from server.serializers import objToSendSerializers
from server.models import requestHistory

class objToSend():
	
	def __init__(self,request):

		self.time = datetime.datetime.now()
		self.method = request.method
		self.headers = allheaders(request.META) 			
		self.path = request.path
		self.query = request.query_params 
		self.body = request.data
		self.duration=request.META.get("duration")


#processing the request, here we can also use class based view but for all the http method we have to perform same task
#so that why using function based view with api_view decorator
@api_view(['POST','GET','PUT','DELETE'])
def process(request):	
	obj=objToSend(request)
	serializer=objToSendSerializers(obj)
	requestObj=request.META.get("requestId")
	requestObj.completed=True
	requestObj.save()
	return Response(serializer.data)


def stat(request):
	return render(request,'index.html',{})
		

class StatDetail(APIView):
	def get(self,request):
		previoushour=timezone.now()-datetime.timedelta(hours=1)
		previousminute=timezone.now()-datetime.timedelta(minutes=1)

		totalrequest=requestHistory.objects.values('method').annotate(Count('method'),Sum('duration'))
		onehourdata=requestHistory.objects.filter(time__range=(previoushour,timezone.now())).values('method').annotate(Count('method'))

		onemindata=requestHistory.objects.filter(time__range=(previousminute,timezone.now())).values('method').annotate(Count('method'))

		activedata=requestHistory.objects.filter(time__range=(
														previousminute,timezone.now()),
														completed=False).values('method').annotate(Count('method'))
		return Response({"totalrequest":totalrequest,"onehourdata":onehourdata,"onemindata":onemindata,"activedata":activedata})
