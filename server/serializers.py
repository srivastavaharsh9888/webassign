from rest_framework import serializers

class objToSendSerializers(serializers.Serializer):
    time = serializers.DateTimeField()
    method = serializers.CharField(max_length=100)
    headers = serializers.DictField()	
    path = serializers.CharField(max_length=200)
    query = serializers.DictField()
    body = serializers.DictField()
    duration=serializers.IntegerField()