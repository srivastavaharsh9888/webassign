from django.db import models

class requestHistory(models.Model):
	method=models.CharField(max_length=100)
	duration=models.IntegerField()
	completed=models.BooleanField(default=0)
	time=models.DateTimeField(auto_now_add=True)