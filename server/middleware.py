import time
import random
from server.models import requestHistory

class TimeDelayMiddleware(object):

    def __init__(self, get_response):
        self.get_response = get_response
        self.delay = random.randint(15,30)

    def __call__(self, request):
        method=request.method
        if request.path.startswith('/process/') and (method=='GET' or method=='POST' or method=='PUT' or method=='DELETE'):
        	requestId=requestHistory.objects.create(method=request.method,duration=self.delay)
        	time.sleep(self.delay)
        	request.META.update({"duration":self.delay,"requestId":requestId})
        response = self.get_response(request)
        return response